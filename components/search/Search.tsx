import React, { useState } from "react";
import SearchBar from "../searchBar/SearchBar";
import SearchResult from "../searchResult/SearchResult";
import styles from "./Search.module.css";

export default function Search() {
  const [term, setTerm] = useState("");

  const onSubmitHandler = (keyword: string) => {
    setTerm(keyword);
  };
  return (
    <div className={styles.search}>
      <SearchBar onSubmit={onSubmitHandler} />
      <SearchResult term={term} />
    </div>
  );
}

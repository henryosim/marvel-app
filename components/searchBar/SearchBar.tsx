import React, { FormEvent, useState } from "react";
import { FaSearch } from "react-icons/fa";
import styles from "./SearchBar.module.css";

interface SearchBarProp {
  onSubmit: (term: string) => void;
}

export default function SearchBar({ onSubmit }: SearchBarProp) {
  const [term, setTerm] = useState("");

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onSubmit(term);
  };
  return (
    <form className={styles.form} onSubmit={handleSubmit}>
      <input
        type="search"
        className={styles.input}
        value={term}
        placeholder="Search for an image"
        onChange={(e) => setTerm(e.target.value)}
      />
      <button className={styles.submitBtn}>
        <FaSearch />
      </button>
    </form>
  );
}

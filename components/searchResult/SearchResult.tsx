import React, { useRef } from "react";
import { SyncLoader } from "react-spinners";
import { useImageSearchData } from "../../hooks/useImageSearchData";

import styles from "./SearchResult.module.css";

export default function SearchResult({ term }: { term: string }) {
  const {
    data,
    isLoading,
    isSuccess,
    hasNextPage,
    fetchNextPage,
    isFetchingNextPage,
    isError,
    error,
  } = useImageSearchData(term);

  const displayError = (e: any) => {
    if (typeof e === "object") {
      return e.message;
    }
    return e;
  };

  if (isLoading) {
    return (
      <div className={styles.loading}>
        <SyncLoader loading={true} color="#007cc2" />
      </div>
    );
  }

  if (isError) {
    return <div className={styles.error}>Error :{displayError(error)}</div>;
  }

  return (
    <div className={styles.searchResult}>
      <div className={styles.pages}>
        {isSuccess &&
          data?.pages.map((page, i) => {
            return page?.data.map((item, key) => (
              <div key={`${key},${key}`} className={styles.imageItem}>
                <img src={item.images.downsized.url} className={styles.image} />
              </div>
            ));
          })}
      </div>

      <button
        className={styles.moreBtn}
        onClick={() => fetchNextPage()}
        disabled={!hasNextPage || isFetchingNextPage}
      >
        load more
      </button>
    </div>
  );
}

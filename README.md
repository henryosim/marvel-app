# Next.js

Marvel app project

## live Demo

https://marvel-app-dusky.vercel.app/

## How to Use

Quickly get started using [Create Next App](https://github.com/vercel/next.js/tree/canary/packages/create-next-app#readme)!

- create a .env file in the project root
- please see accompanying email for contents of .env file

In your terminal, run the following commands to install dependencies and run the dev server

```bash
yarn install
# or
yarn dev
```

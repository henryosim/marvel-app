import Head from "next/head";
import Image from "next/image";

import styles from "@/pages/index.module.css";
import Search from "@/components/search/Search";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Marvel App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Search />
    </div>
  );
}

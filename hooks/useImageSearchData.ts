import axios from "axios";
import { SearchResponse } from "interfaces/search";
import { QueryFunctionContext, useInfiniteQuery } from "react-query";

const apiClient = axios.create({
  baseURL: process.env.NEXT_PUBLIC_GIFFY_API_URL,
  params: {
    api_key: process.env.NEXT_PUBLIC_GIPHY_API_KEY,
  },
});

const getNextPageParam = (page: SearchResponse) => {
  const { count, offset, total_count } = page.pagination;
  const nextOffset = offset < total_count ? (offset + 1) * count : undefined;

  return nextOffset;
};

export const useImageSearchData = (term: string = "") => {
  const fetchMoreImageSearchData = async (context: QueryFunctionContext) => {
    const path = "gifs/search";
    const q = term;
    const offset = context.pageParam;
    const limit = 10;

    try {
      const resp = await apiClient.get(path, { params: { q, offset, limit } });
      return resp.data;
    } catch (err: any) {
      throw new Error(err);
    }
  };

  return useInfiniteQuery(["searchImages", term], fetchMoreImageSearchData, {
    getNextPageParam,
  });
};

import { render, screen } from "@testing-library/react";
import Home from "@/pages/index";
import { QueryClient, QueryClientProvider } from "react-query";
import { createWrapper } from "../test/utils";

describe("Home", () => {
  it("renders input form", () => {
    render(<Home />, { wrapper: createWrapper() });

    const searchInput = screen.getByRole("searchbox");
    const searchBtn = screen.getByRole("button");

    expect(searchInput).toBeInTheDocument();
    expect(searchBtn).toBeInTheDocument();
  });
});

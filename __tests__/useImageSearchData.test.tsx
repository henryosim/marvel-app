import { useImageSearchData } from "../hooks/useImageSearchData";

import { renderHook, act } from "@testing-library/react-hooks/dom"; // will use react-dom

import { createWrapper } from "../test/utils";
import { server } from "../jest.setup";
import { rest } from "msw";

describe("query hook", () => {
  test("successful query hook", async () => {
    const { result, waitFor } = renderHook(() => useImageSearchData(), {
      wrapper: createWrapper(),
    });

    await waitFor(() => result.current.isSuccess);

    expect(result.current.data?.pages.length).toBeGreaterThan(0);
  });

  test("failure query hook", async () => {
    server.use(
      rest.get("*", (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    const { result, waitFor } = renderHook(() => useImageSearchData(), {
      wrapper: createWrapper(),
    });

    await waitFor(() => result.current.isError);

    expect(result.current.data?.pages.length).toEqual(0);
  });
});

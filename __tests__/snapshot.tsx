import { render } from "@testing-library/react";
import Home from "@/pages/index";
import { createWrapper } from "../test/utils";

it("renders homepage unchanged", () => {
  const { container } = render(<Home />, { wrapper: createWrapper() });
  expect(container).toMatchSnapshot();
});

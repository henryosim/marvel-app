import { render } from "@testing-library/react";
import { rest } from "msw";
import * as React from "react";
import { QueryClient, QueryClientProvider } from "react-query";

export const handlers = [
  rest.get("*/gifs/search", (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        pageParams: [],
        pages: [
          {
            data: [
              {
                type: "gif",
                id: "8h5LtvPAvBHs4",
                url: "https://giphy.com/gifs/8h5LtvPAvBHs4",
                slug: "8h5LtvPAvBHs4",
                bitly_gif_url: "http://gph.is/299CyKN",
                bitly_url: "http://gph.is/299CyKN",
                embed_url: "https://giphy.com/embed/8h5LtvPAvBHs4",
                username: "",
                source: "http://imgur.com/gallery/E9Ecrcm",
                title: "henry GIF",
                rating: "g",
                content_url: "",
                source_tld: "imgur.com",
                source_post_url: "http://imgur.com/gallery/E9Ecrcm",
                is_sticker: 0,
                import_datetime: "2016-06-25 19:10:30",
                trending_datetime: "1970-01-01 00:00:00",
                images: {
                  original: {
                    height: "162",
                    width: "300",
                    size: "1260905",
                    url: "https://media2.giphy.com/media/8h5LtvPAvBHs4/giphy.gif?cid=ab4cfaf5awrpusz1ygx0flew78jll129798rk0wdrchieuns&rid=giphy.gif&ct=g",
                    mp4_size: "896209",
                    mp4: "https://media2.giphy.com/media/8h5LtvPAvBHs4/giphy.mp4?cid=ab4cfaf5awrpusz1ygx0flew78jll129798rk0wdrchieuns&rid=giphy.mp4&ct=g",
                    webp_size: "857714",
                    webp: "https://media2.giphy.com/media/8h5LtvPAvBHs4/giphy.webp?cid=ab4cfaf5awrpusz1ygx0flew78jll129798rk0wdrchieuns&rid=giphy.webp&ct=g",
                    frames: "128",
                    hash: "335038b80260e6547211e576543a8867",
                  },
                },
              },
            ],
            pagination: {
              total_count: 0,
              count: 0,
              offset: 0,
            },
            meta: {
              status: 200,
              msg: "OK",
              response_id: "1shxj6zh11alfd6xaxx5qf62axe4665pip2wlefe",
            },
          },
        ],
      })
    );
  }),
  rest.get("*/gifs/search/*", (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        name: "mocked-react-query",
      })
    );
  }),
];

const createTestQueryClient = () =>
  new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
      },
    },
  });

export function renderWithClient(ui: React.ReactElement) {
  const testQueryClient = createTestQueryClient();
  const { rerender, ...result } = render(
    <QueryClientProvider client={testQueryClient}>{ui}</QueryClientProvider>
  );
  return {
    ...result,
    rerender: (rerenderUi: React.ReactElement) =>
      rerender(
        <QueryClientProvider client={testQueryClient}>
          {rerenderUi}
        </QueryClientProvider>
      ),
  };
}

export function createWrapper() {
  const testQueryClient = createTestQueryClient();
  return ({ children }: { children: React.ReactNode }) => (
    <QueryClientProvider client={testQueryClient}>
      {children}
    </QueryClientProvider>
  );
}
